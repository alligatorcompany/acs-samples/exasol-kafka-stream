import json
from os import environ
from time import sleep

import pyexasol
from kafka3 import KafkaAdminClient, KafkaProducer
from kafka3.admin import NewTopic

if __name__ == "__main__":

    schema_name = environ.get("EXASOL_SCHEMA")
    relation_name = environ.get("EXASOL_RELATION")
    brokers = environ.get("BROKERS", "")
    topic_name = f"{schema_name}__{relation_name}"

    producer = KafkaProducer(bootstrap_servers=brokers)
    try:
        # Create Kafka topic
        topic = NewTopic(name=topic_name, num_partitions=5, replication_factor=1)
        admin = KafkaAdminClient(bootstrap_servers=brokers)
        admin.create_topics([topic])
    except Exception:
        print(f"Topic {topic_name} is already created")

    C = pyexasol.connect(
        dsn=environ.get("EXASOL_DSN", "localhost:8563"),
        user=environ.get("EXASOL_USER", "sys"),
        password=environ.get("EXASOL_PASSWORD", "exasol"),
        fetch_dict=True,
    )
    stmt = C.execute(f"select * from {schema_name}.{relation_name}")
    for count, row in enumerate(stmt):
        if count > 0 and count % 10000 == 0:
            print(f"Fetched {count} rows. Waiting 5 minutes.")
            sleep(300)
        producer.send(
            topic_name, json.dumps({k.lower(): v for k, v in row.items()}).encode()
        )

    C.close()
    producer.close()
