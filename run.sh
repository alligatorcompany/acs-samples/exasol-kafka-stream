#!/bin/bash
NS=dev-torsten

kubectl --namespace $NS run -i --tty --rm --restart=Never rpk-producer \
--image=alligatorcompany/testdata:latest \
--env="EXASOL_PASSWORD=$EXASOL_PASSWORD" \
--env="EXASOL_DSN=$EXASOL_DSN" \
--env="EXASOL_USER=$EXASOL_USER" \
--env="EXASOL_SCHEMA=$EXASOL_SCHEMA" \
--env="EXASOL_RELATION=$EXASOL_RELATION" \
--env="BROKERS=$BROKERS" \
--command bash
