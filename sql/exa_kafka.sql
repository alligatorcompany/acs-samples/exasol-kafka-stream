CREATE SCHEMA KAFKA_EXTENSION;

OPEN SCHEMA KAFKA_EXTENSION;

CREATE OR REPLACE JAVA SET SCRIPT KAFKA_CONSUMER(...) EMITS (...) AS
  %scriptclass com.exasol.cloudetl.kafka.KafkaConsumerQueryGenerator;
  %jar /buckets/bfsdefault/default/exasol-kafka-connector-extension-1.5.3.jar;
/

CREATE OR REPLACE JAVA SET SCRIPT KAFKA_IMPORT(...) EMITS (...) AS
  %scriptclass com.exasol.cloudetl.kafka.KafkaTopicDataImporter;
  %jar /buckets/bfsdefault/default/exasol-kafka-connector-extension-1.5.3.jar;
/

CREATE OR REPLACE JAVA SET SCRIPT KAFKA_METADATA(
    PARAMS VARCHAR(2000),
    KAFKA_PARTITION DECIMAL(18, 0),
    KAFKA_OFFSET DECIMAL(36, 0)
)
EMITS (PARTITION_INDEX DECIMAL(18, 0), MAX_OFFSET DECIMAL(36, 0)) AS
  %scriptclass com.exasol.cloudetl.kafka.KafkaTopicMetadataReader;
  %jar /buckets/bfsdefault/default/exasol-kafka-connector-extension-1.5.3.jar;
/


CREATE SCHEMA YYY_Q3_PSA;

CREATE OR REPLACE TABLE YYY_Q3_PSA.PSA_STREAM_RAW_MARKETO__MAR_SEND_MAIL (
    PAYLOAD VARCHAR(2000000),
    KAFKA_PARTITION DECIMAL(18, 0),
    KAFKA_OFFSET DECIMAL(36, 0)
);

IMPORT INTO YYY_Q3_PSA.PSA_STREAM_RAW_MARKETO__MAR_SEND_MAIL
FROM SCRIPT KAFKA_EXTENSION.KAFKA_CONSUMER WITH
BOOTSTRAP_SERVERS = 'test-cluster-0.test-cluster.redpanda-system.svc.cluster.local:9092'
SCHEMA_REGISTRY_URL = 'test-cluster-0.test-cluster.redpanda-system.svc.cluster.local:8081'
TOPIC_NAME = 'raw_marketo__mar_send_email'
TABLE_NAME = 'YYY_Q3_PSA.PSA_STREAM_RAW_MARKETO__MAR_SEND_MAIL'
RECORD_VALUE_FORMAT = 'json';

SELECT * FROM YYY_Q3_PSA.PSA_STREAM_RAW_MARKETO__MAR_SEND_MAIL LIMIT 100;

TRUNCATE TABLE YYY_Q3_PSA.PSA_STREAM_RAW_MARKETO__MAR_SEND_MAIL;
